/*
 * ExtendedObjectTracker.h
 *
 * Code generation for model "ExtendedObjectTracker".
 *
 * Model version              : 1.3
 */

#ifndef RTW_HEADER_ExtendedObjectTracker_h_
#define RTW_HEADER_ExtendedObjectTracker_h_
#include <string.h>
#include <stddef.h>
#ifndef ExtendedObjectTracker_COMMON_INCLUDES_
#define ExtendedObjectTracker_COMMON_INCLUDES_
#include "rtwtypes.h"
#endif                   /* ExtendedObjectTracker_COMMON_INCLUDES_ */

#include "ExtendedObjectTracker_types.h"


/* Macros for accessing real-time model data structure */
#ifndef rtmGetErrorStatus
#define rtmGetErrorStatus(rtm)         ((rtm)->errorStatus)
#endif

#ifndef rtmSetErrorStatus
#define rtmSetErrorStatus(rtm, val)    ((rtm)->errorStatus = (val))
#endif

#define ExtendedObjectTracker_M (Perception_Object_Classifica_M)

/* External inputs (root inport signals with default storage) */
typedef struct {
  DetectedObjects Detected_Objects;    /* '<Root>/Detected_Objects' */
} ExtU_Perception_Object_Classi_T;

/* External outputs (root outports fed by signals with default storage) */
typedef struct {
  float Type_CarIDs[50];              /* '<Root>/Type_CarIDs' */
  float counter_ofCars;               /* '<Root>/counter_ofCars' */
  float Type_BicycleIDs[50];          /* '<Root>/Type_BicycleIDs' */
  float counter_ofBicycles;           /* '<Root>/counter_ofBicycles' */
  float Type_PedestrianIDs[50];       /* '<Root>/Type_PedestrianIDs' */
  float counter_ofPedestrians;        /* '<Root>/counter_ofPedestrians' */
  float Type_TruckIDs[50];            /* '<Root>/Type_TruckIDs' */
  float counter_ofTrucks;             /* '<Root>/counter_ofTrucks' */
  float Type_OtherIDs[50];            /* '<Root>/Type_OtherIDs' */
  float counter_ofOthers;             /* '<Root>/counter_ofOthers' */
} ExtY_Perception_Object_Classi_T;

/* Real-time Model Data Structure */
struct tag_RTM_Perception_Object_Cla_T {
  const char *errorStatus;
};

/* External inputs (root inport signals with default storage) */
extern ExtU_Perception_Object_Classi_T Perception_Object_Classificat_U;

/* External outputs (root outports fed by signals with default storage) */
extern ExtY_Perception_Object_Classi_T Perception_Object_Classificat_Y;

/* External data declarations for dependent source files */
extern const DetectedObjects Perception_Object_Classificatio;/* DetectedObjects ground */

/* Model entry point functions */
extern void ExtendedObjectTracker_initialize(void);
extern void ExtendedObjectTracker_step(void);
extern void ExtendedObjectTracker_terminate(void);

/* Real-time Model object */
extern RT_MODEL_Perception_Object_Cl_T *const Perception_Object_Classifica_M;

/*-
 * The generated code includes comments that allow you to trace directly
 * back to the appropriate location in the model.  The basic format
 * is <system>/block_name, where system is the system number (uniquely
 * assigned by Simulink) and block_name is the name of the block.
 *
 * Use the MATLAB hilite_system command to trace the generated code back
 * to the model.  For example,
 *
 * hilite_system('<S3>')    - opens system 3
 * hilite_system('<S3>/Kp') - opens and selects block Kp which resides in S3
 *
 * Here is the system hierarchy for this model
 *
 * '<Root>' : 'ExtendedObjectTracker'
 * '<S1>'   : 'ExtendedObjectTracker/Classification'
 */
#endif                      /* RTW_HEADER_ExtendedObjectTracker_h_ */
